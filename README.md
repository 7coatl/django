# Django

My template for django

## Prerequisitos

Instalar

Git - https://git-scm.com/downloads
Docker - https://www.docker.com/products/docker-desktop


## Descargar el repositorio

Ir al directorio donde se almacenara el proyecto y poner la siguiente instrucción

```
git clone https://gitlab.com/7coatl/django.git
```

## Crear el proyecto de django

Ingresar al directorio django y poner la siguiente instrucción, debe llevar punto al final. reemplazar <nombre del proyecto> por el nombre del proyecto.

```
sudo docker-compose run web django-admin startproject <nombre del proyecto> .
```

## Configurar la conexión a la base de datos en django

Cambiar en en <nombre del proyecto>/settings.py la sección de DATABASES por el siguiente código, no hay que olvidar import os al principio del archivo.

```
# settings.py

import os

[...]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_NAME'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

## Correr servidor Django

Para correr el servidor de django utilizamos la siguiente instrucción

```
docker-compose up
```

Probar qu este corriendo poniendo la siguiente dirección en el navegador
http://localhost:8000
